/*
  MIDI note player


  DIN circuit:
    - digital in 1 connected to MIDI jack pin 5
    - MIDI jack pin 2 connected to ground
    - MIDI jack pin 4 connected to +5V through 220 ohm resistor
    - Attach a MIDI cable to the jack, then to a MIDI synth, and play music.

 Piezo circuit:
    - one lead of piezo connected to ground, other to analog in
    - 1 M Ohm resister connected to ground and same analog in
    
  http://www.arduino.cc/en/Tutorial/Midi

10000000 = 0x80 = note off
10010000 = 0x90 = note on
10100000 = 0xA0 = aftertouch
10110000 = 0xB0 = continuous controller
11000000 = 0xC0 = patch change
11010000 = 0xD0 = channel pressure
11100000 = 0xE0 = pitch bend
11110000 = 0xF0 = non-musical commands
*/

bool playingNote=false;
unsigned char noteThreshold = 30;
const int selectPins[4] = {2, 3, 4, 5}; // S0~2, S1~3, S2~4

// following stolen from https://www.instructables.com/id/Arduino-Xylophone/
byte PadNote[16] = {
  57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72};         // MIDI notes from 0 to 127 (Mid C = 60)
int PadCutOff[16] = 
{
  30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30};           // Minimum Analog value to cause a drum hit
int MaxPlayTime[16] = {
  90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90};               // Cycles before a 2nd hit is allowed
#define  midichannel 1;                              // MIDI channel from 0 to 15 (+1 in "real world")
boolean VelocityFlag  = true;                           // Velocity ON (true) or OFF (false)

//*******************************************************************************************************************
// Internal Use Variables
//*******************************************************************************************************************
boolean activePad[16] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};                   // Array of flags of pad currently playing
int PinPlayTime[16] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};                     // Counter since pad started to play
 byte status1;
int hitavg = 0;
const int knobInput = A5;
int sensitivityOffset = 1023;

void setup() {
  // Set MIDI baud rate:
  Serial.begin(31250);
  //Serial.begin(9600); // baud rate for using the serial console
    for (int i=0; i<4; i++)
  {
    pinMode(selectPins[i], OUTPUT);
    digitalWrite(selectPins[i], HIGH);
  }
  
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  //loopNotes(); // loops through all notes and sends through midi
  //testPiezo(); // diplays the piezo output values
  //playPiezoNote(); // when piezo is activated, plays a single midi note
  multiplexerTest();
  //delay(5);
}

int readKnob()
{
  return analogRead(knobInput);
//  Serial.print("knob value: ");
//  Serial.print(value);
//  Serial.print("\n");
}

// reads from analog pin and returns value from 0-255
unsigned char getVelocity(unsigned char pin = A0)
{
    int val = analogRead(A0);
    //Serial.print("value: ");
    //Serial.print(val);
    if (val > sensitivityOffset + 1)
      return 255;
    return  (unsigned char)map(val, 0, sensitivityOffset + 1, 0, 255);
}


void playPiezoNote(byte note = 0x3c)
{
    unsigned char velocity = getVelocity(A0);
    noteOn(note, velocity);
    //digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    //delay(10);
    //digitalWrite(LED_BUILTIN, LOW);   // turn the LED on (HIGH is the voltage level)

}

void testPiezo()
{
  //int val = analogRead(A0);
  //int mappedVal = map(val, 0, 1024, 0, 255);
  //Serial.print("value: ");
  //Serial.print(val);
  
  unsigned char mappedVal = getVelocity();

  Serial.print(" mapped: ");
  Serial.print(mappedVal);
  if (mappedVal > noteThreshold)
  {
    Serial.print("greater");
   digitalWrite(LED_BUILTIN, HIGH);  
  }
  else
  {
    Serial.print("less than");
    digitalWrite(LED_BUILTIN, LOW);   
  }
    Serial.print("\n\n");
  delay(100);
}


void loopNotes()
{
    // play notes from F#-0 (0x1E) to F#-5 (0x5A):
  for (unsigned char note = 0x1E; note < 0x5A; note ++) {
    //Note on channel 1 (0x90), some note value (note), middle velocity (0x45):
    noteOn(note, 0x45);
    delay(100);
    //Note on channel 1 (0x90), some note value (note), silent velocity (0x00):
    noteOn(note, 0x00);
    delay(100);
  }
}

// plays a MIDI note. 
void noteOn( unsigned char pitch, unsigned char velocity) {
  Serial.write(0x90);
  Serial.write(pitch);
  Serial.write(velocity);
}

void noteOff(unsigned char pitch, unsigned char velocity)
{
    Serial.write(0x80);
    Serial.write(pitch);
    Serial.write(velocity);
}

// We won't use for the xylophone, but you can pitch bend
void pitchBend(unsigned char msb, unsigned char lsb)
{
  Serial.write(0xE0);
  Serial.write(lsb);
  Serial.write(msb);
}


// The selectMuxPin function sets the S0, S1, and S2 pins
// accordingly, given a pin from 0-7.
void selectMuxPin(byte pin)
{
  for (int i=0; i<4; i++)
  {
    if (pin & (1<<i))
      digitalWrite(selectPins[i], HIGH);
    else
      digitalWrite(selectPins[i], LOW);
  }
}

void multiplexerTest()
{
  sensitivityOffset = readKnob();
  for (byte selection = 0; selection < 4; selection++)
  {
    selectMuxPin(selection);
    hitavg = getVelocity(A0); // when we add more "banks" we'll need to go through each analog pin 
    if (hitavg > PadCutOff[selection])
    {
      if(!activePad[selection])
      {
        if(!VelocityFlag)
        {
          hitavg = 127;
        }
        noteOn(PadNote[selection], hitavg);
        PinPlayTime[selection] = 0;
        activePad[selection] = true;
      }
      else
      {
        PinPlayTime[selection]++;
      }
    }
    else if(activePad[selection])
    {
      PinPlayTime[selection]++;
      if(PinPlayTime[selection] > MaxPlayTime[selection])
      {
        activePad[selection] = false;
        noteOn(PadNote[selection], 0);
      }
    }
    //playPiezoNote(0x3c + selection);
    
//    Serial.print("selection: ");
//    Serial.print(selection);
//    unsigned char mappedVal = getVelocity(A0);
//    Serial.print(" mappedVal: ");
//    Serial.print(mappedVal);
//    Serial.print("\n");
    //delay(10);
  }
}

//void noloop() 
//{
//  for(int pin=0; pin < 16; pin++)                          //
//  {
//    //int pin = 3;
//    //   for (pinRead=0; pinRead < 16, pin++){
//    hitavg = analogRead(pinAssignments[pin]);  
//    //Serial.println(hitavg);   
//    // read the input pin
//
//    if((hitavg > PadCutOff[pin]))
//    {
//      if((activePad[pin] == false))
//      {
//        if(VelocityFlag == true)
//        {
//          //          hitavg = 127 / ((1023 - PadCutOff[pin]) / (hitavg - PadCutOff[pin]));    // With full range (Too sensitive ?)
//          hitavg = (hitavg / 8) -1 ;                                                 // Upper range
//        }
//        else
//        {
//          hitavg = 127;
//        }
//        MIDI_TX(144,PadNote[pin],hitavg); //note on
//
//        PinPlayTime[pin] = 0;
//        activePad[pin] = true;
//      }
//      else
//      {
//        PinPlayTime[pin] = PinPlayTime[pin] + 1;
//      }
//    }
//    else if((activePad[pin] == true))
//    {
//      PinPlayTime[pin] = PinPlayTime[pin] + 1;
//      if(PinPlayTime[pin] > MaxPlayTime[pin])
//      {
//        activePad[pin] = false;
//        MIDI_TX(144,PadNote[pin],0); 
//      }
//    }
//  } 
//}
